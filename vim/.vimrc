" vim-plug automatic installation
let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

filetype plugin on

" plugins
call plug#begin('~/.vim/plugged')

Plug 'sheerun/vim-polyglot'
Plug 'sainnhe/everforest'
Plug 'preservim/nerdtree'
Plug 'vim-airline/vim-airline'

call plug#end()

colorscheme everforest

syntax on
set hlsearch
set autoindent
set tabstop=4
set ignorecase
set incsearch
set smartcase
set wrap
set number relativenumber
set spell
